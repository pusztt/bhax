#include <stdio.h>
#include <curses.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
int
main ( void )
{
    WINDOW *ablak;
    ablak = initscr ();

    int x = 0;
    int y = 0;

    int xnov = 1;
    int ynov = 1;

    int mx;
    int my;

    for (;;)
    {
        getmaxyx(ablak, my, mx);

        mvprintw(y, x, "O");

        refresh();
        usleep(100000);

        clear();

	xnov=(int)floor((x+1)/mx)*(-2)+1;
	ynov=(int)floor((y+1)/my)*(-2)+1;

        x = x + xnov;
        y = y + ynov;
    }

    return 0;
}
