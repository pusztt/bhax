	A probléma egy létező vetélkedő műsorvezetőjéről kapta a nevét. A műsor végén a játékosnak mutatnak 3 ajtót, mely közül 2 mögött kecske van, a 3. mögött pedig egy autó. A játékos választ, majd a műsorvezető megmutat a maradék 2 ajtó közül egy olyat, mely mögött kecske van, majd lehetőséget ad a játékosnak a változtatásra. Habár elsőre mindenki azt gondolná, mindegy, hogy változtatunk-e, hiszen 50%-50% az esélye mindkét ajtónak, hogy az autót rejt, valójában nagyobb esélyünk van nyerni, ha változtatunk. Az első választásnál 1/3 az esélye a mi ajtónknak, hogy autót rejt, és 2/3 az esélye a másik kettőnek összesen. Az egyik ajtó felfedése után, az eredeti esélyek nem változnak, a nyitott ajtó, és az általunk nem választott ajtó esélye összesen még mindig 2/3. Mivel a nyitva lévő ajtó mögött kecske van, ezért az általunk nem választott ajtó mögött 2/3 eséllyel lesz autó, ami több, mint az általunk választotté, ami 1/3. A kísérlet R nyelvbeli szimulációja:
	<para/>
	<programlisting language="R"><![CDATA[
	#   An illustration written in R for the Monty Hall Problem 
#   Copyright (C) 2019  Dr. Norbert Bátfai, nbatfai@gmail.com
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

kiserletek_szama=1000
kiserlet = sample(1:3, kiserletek_szama, replace=T)
jatekos = sample(1:3, kiserletek_szama, replace=T)
musorvezeto=vector(length = kiserletek_szama)

for (i in 1:kiserletek_szama) {

    if(kiserlet[i]==jatekos[i]){
    
        mibol=setdiff(c(1,2,3), kiserlet[i])
    
    }else{
    
        mibol=setdiff(c(1,2,3), c(kiserlet[i], jatekos[i]))
    
    }

    musorvezeto[i] = mibol[sample(1:length(mibol),1)]

}

nemvaltoztatesnyer= which(kiserlet==jatekos)
valtoztat=vector(length = kiserletek_szama)

for (i in 1:kiserletek_szama) {

    holvalt = setdiff(c(1,2,3), c(musorvezeto[i], jatekos[i]))
    valtoztat[i] = holvalt[sample(1:length(holvalt),1)]
    
}

valtoztatesnyer = which(kiserlet==valtoztat)


sprintf("Kiserletek szama: %i", kiserletek_szama)
length(nemvaltoztatesnyer)
length(valtoztatesnyer)
length(valtoztatesnyer)/length(nemvaltoztatesnyer)
length(nemvaltoztatesnyer)+length(valtoztatesnyer)
